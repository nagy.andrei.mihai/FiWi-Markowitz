import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.util.List;

/**
 * Created by Andrei Nagy on 23.01.2017.
 */
public class ScatterChartMarkowitz {
    public static void display(Stage stage, String tickerAsset1, String tickerAsset2, String tickerAsset3, List<Double> points_asset1, List<Double> points_asset2, List<Double> points_asset3, double[] mvp) {
        stage.setTitle("Graph Markowitz");
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Risk / Volatility");
        yAxis.setLabel("Yield / Return");
        final ScatterChart<Number, Number> scatterChart = new ScatterChart<Number, Number>(xAxis, yAxis);

        scatterChart.setTitle("Markowitz Model FiWi");

        XYChart.Series asset1 = new XYChart.Series();
        XYChart.Series asset2 = new XYChart.Series();
        XYChart.Series asset3 = new XYChart.Series();
        XYChart.Series mvp_point = new XYChart.Series();
        asset1.setName(tickerAsset1);
        asset2.setName(tickerAsset2);
        asset3.setName(tickerAsset3);
        mvp_point.setName("Minimum Variance Portfolio Point");

        for (int i = 0; i < points_asset1.size() - 1; i = i + 2)
            asset1.getData().add(new XYChart.Data(points_asset1.get(i + 1), points_asset1.get(i)));

        for (int i = 0; i < points_asset2.size() - 1; i = i + 2)
            asset2.getData().add(new XYChart.Data(points_asset2.get(i + 1), points_asset2.get(i)));

        for (int i = 0; i < points_asset3.size() - 1; i = i + 2)
            asset3.getData().add(new XYChart.Data(points_asset3.get(i + 1), points_asset3.get(i)));

        mvp_point.getData().add(new XYChart.Data(mvp[1], mvp[0]));

        System.out.println("\n\nMVP:\nReturn: " + mvp[1] + "\nVolatility: " + mvp[0]);
        System.out.println("\nPortfolio Value-at-Risk 95% (mit je 33,3%)" + MarkowitzModelFiWi.getPortfolioValueAtRisk(0.95, 0.33, 0.34, 0.33));

        Scene scene = new Scene(scatterChart, 800, 600);
        //scatterChart.getData().addAll(asset1, asset2, asset3);
        scatterChart.getData().add(asset1);
        scatterChart.getData().add(asset2);
        scatterChart.getData().add(asset3);
        scatterChart.getData().add(mvp_point);

        stage.setScene(scene);
        stage.show();
    }
}
